FROM centos:centos6.6

run yum -y update ; yum clean all
run yum -y install yum-utils; yum clean all
run yum -y groupinstall development; yum clean all

#run yum -y install centos-release-scl; yum clean all
#run yum -y install rh-python36; yum clean all
#run scl enable rh-python36 bash

run yum -y install wget tar bzip2; yum clean all
run wget https://repo.continuum.io/archive/Anaconda3-4.3.1-Linux-x86_64.sh; yum clean all
run bash Anaconda3-4.3.1-Linux-x86_64.sh -b -f; yum clean all
#run source /root/anaconda3/bin/activate; yum clean all

run wget https://cmake.org/files/v3.6/cmake-3.6.2.tar.gz && \
    tar -zxvf cmake-3.6.2.tar.gz && \
    cd cmake-3.6.2 && \
    ./bootstrap --prefix=/usr/local && \
    make && \
    make install && \
    echo PATH=/usr/local/bin:$PATH:$HOME/bin >> ~/.bash_profile \
    ; yum clean all



# sudo docker build -t registry.windenergy.dtu.dk/hawc2private/hawclicense -f jess_dockerfile .
# sudo docker run -it registry.windenergy.dtu.dk/hawc2private/hawclicense bash
# sudo docker push registry.windenergy.dtu.dk/hawc2private/hawclicense
