# Jess

Docker image to build applications for Jess
- Centos6.6
 
Additional installed:
- Anaconda
- cmake 3.6

## Use image
image: registry.windenergy.dtu.dk/dockerimages/jess


## Build and push image

See https://gitlab.windenergy.dtu.dk/dockerimages/jess/container_registry

MMPE(2019-12-04)I did not succeed in automatic ci build (and push)
